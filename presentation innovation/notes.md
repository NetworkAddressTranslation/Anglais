3d printers

# Definition
addictive manufacturing
Creating a 3d object by adding layers of material under computer control

# Printing process

## Before printing
* creating a 3d model of the object
* checking for errors in the file, such as holes
* slicing the 3d model (converting it into a series of layers) for the 3d printer

## After printing
* painting
* remove internals

# Uses


